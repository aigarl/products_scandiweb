<?php
// Checking and submitting input product data

require_once("../src/Book.php");
require_once("../src/DVDDisc.php");
require_once("../src/Furniture.php");
	
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$type_objects = ["dvddisc" => new DVDDisc(), "book" => new Book(), "furniture" => new Furniture()];
	
	$data = $_POST;
	
	if (array_key_exists($_POST["type"], $type_objects)) {
		$answer = $type_objects[$_POST["type"]]->checkData($data);
		if ($answer === []) {
			$type_objects[$_POST["type"]]->submitData();
		}
		echo(json_encode($answer, JSON_FORCE_OBJECT));
	} else {
		echo(json_encode(["type" => "Choose the product type."]));
	}
}
