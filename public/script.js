function changeAdditionalInfo(type_name) {
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.querySelector(".dynamic_input_group").innerHTML = this.responseText;
		}
	};
	xhttp.open("GET", "change.php?type=" + type_name, true);
	xhttp.send();
}

function submitData() {
	let data = document.forms['form']
	let elements = document.getElementsByClassName("form-group");
	for (let i = 0; i < elements.length; ++i) {
		elements[i].classList.remove("has-error");
	}
	elements = document.getElementsByClassName("help-block error");
	for (i = 0; i < elements.length; ++i) {
		elements[i].innerHTML = "";
	}
	
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			let answer = JSON.parse(this.responseText);
			console.log(answer);
			if (Object.keys(answer).length === 0) {
				alert("Success.");
				open("/index.php", "_self");
			} else {
				alert("Errors in input data.");
				Object.keys(answer).forEach( function (key) {
					let input = document.getElementById(key);
					input.querySelector("span.help-block").innerHTML = this[key];
					input.classList.add("has-error");
				}, answer);
			}
		}
	};
	xhttp.open("POST", "submit.php", true);
	xhttp.send(new FormData(data));
}

function deleteData() {
	let deleteChecks = document.querySelectorAll("input[name='delete']");
	let deleteIds = [];
	for (let i = 0; i < deleteChecks.length; ++i) {
		if (deleteChecks[i].checked) {
			deleteIds.push(deleteChecks[i].value);
		}
	}
	
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			let answer = this.responseText;
			if (answer == "success") {
				for (let i = 0; i < deleteIds.length; ++i) {
					let element = document.querySelector("div[data-pid='" + deleteIds[i] + "']");
					element.parentElement.removeChild(element);
				}
			}
		}
	};
	
	xhttp.open("POST", "delete.php", true);
	xhttp.send(JSON.stringify(deleteIds));
	
}