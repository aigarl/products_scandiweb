<!DOCTYPE html>
<html>
	<head>
		<title>Product list</title>
		<meta charset="utf-8"> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<script src="script.js"></script>
	</head>
	<body>
	<div class="container">
		<div class="page-header">
			<div class="row">
				<div class="col-xs-8"><h1>Product List</h1></div>
				<div class="col-xs-4 button_container"><a onclick="deleteData()" class="btn btn-default">Delete</a> <a href="add.php" class="btn btn-default">Add Product</a></div></div>
			</div>
		</div>
	</div>
	<div class="container">
		<?php 
			require_once("../src/CreateTable.php");
			echo(create_table());
		?>
	</div>
	</body>
</html>