<?php
// Deleteing selected products

require_once("../src/Database.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$data = json_decode(file_get_contents("php://input"), true);
	$connection = Database::connect();
	
	$statement = $connection->prepare("DELETE FROM product WHERE id = ?");
	
	foreach ($data as $id) {
		if (is_numeric($id)) {
			$statement->bind_param("i", $id);
			$statement->execute();
		}
	}
	
	
	echo("success");
	
}