<!DOCTYPE html>
<html>
	<head>
		<title>Add product</title>
		<meta charset="utf-8"> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<script src="script.js"></script>
	</head>
	<body>
	<div class="container">
		<div class="page-header">
			<div class="row">
				<div class="col-xs-8"><h1>Add product</h1></div>
				<div class="col-xs-4 button_container"><div class="buttons"><form><button type="button" class="btn btn-default" onclick="submitData()">Save</button></form></div></div>
			</div>
		</div>
	</div>
		<div class="container">
			<div class="row">
				<div class="col-sm-5 col-lg-4">
					<form class="form-horizontal" id="form">
						<div class="form-group" id="sku">
							<label class="control-label col-sm-3" for="sku">SKU</label>
							<div class="col-sm-9"><input type="text" class="form-control" name="sku"></input>
							<span class="help-block error"></span></div>
						</div>
						<div class="form-group" id="name">
							<label class="control-label col-sm-3" for="name">Name</label>
							<div class="col-sm-9"><input type="text" class="form-control" name="name"></input>
							<span class="help-block error"></span></div>
						</div>
						<div class="form-group" id="price">
							<label class="control-label col-sm-3" for="price">Price</label>
							<div class="col-sm-9"><input type="text" class="form-control" name="price"></input>
							<span class="help-block error"></span></div>
						</div>
						<div class="form-group" id="type">
							<label class="control-label col-sm-3" for="type">Type</label>
							<div class="col-sm-9"><select class="form-control" name="type" onchange="changeAdditionalInfo(this.value)">
								<option value="none">None</option>
								<option value="book">Book</option>
								<option value="dvddisc">DVD Disc</option>
								<option value="furniture">Furniture</option>
							</select>
							<span class="help-block error"></span></div>
						</div>
						<div class = "panel panel-default">
							<div class="dynamic_input_group">
								
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>