<?php
// Changing the dynamic part of the product form

require_once("../src/Book.php");
require_once("../src/DVDDisc.php");
require_once("../src/Furniture.php");

$type_objects = ["dvddisc" => new DVDDisc(), "book" => new Book(), "furniture" => new Furniture()];

$type_name = htmlspecialchars(stripslashes($_REQUEST["type"]));

if (array_key_exists($type_name, $type_objects)) {
	echo($type_objects[$type_name]->askData());
} else {
	echo('');
}