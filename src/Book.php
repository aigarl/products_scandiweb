<?php
// Book type class

require_once("Product.php");
require_once("Database.php");

class Book extends Product
{
	public $weight;
	
	public function setExtraData($data)
	{
		$this->weight = $data["weight"];
	}
	
	function askData()
	{
		$str = '
		<div class="form-group" id="weight">
			<label class="control-label col-sm-3" for="weight">Weight</label>
			<div class="col-sm-9">
			<input type="text" class="form-control" name="weight"></input>
			<span class="help-block error"></span>
			</div>
		</div>
		<div class="additional_info">
			Please, type the weight of the book in grams.
		</div>
		';
		echo($str);
	}
	
	public function checkData(& $data)
	{
		$answer = parent::checkData($data);
		
		if (!empty($data["weight"])) {
			$this->weight = $this->cleanData($data["weight"]);
			if (!is_numeric($this->weight)) {
				$answer["weight"] = "Weight should be numeric";
			} else if ($this->weight < 0) {
				$answer["weigth"] = "Weigth can't be negative";
			}
		} else {
			$answer["weight"] = "Obligatory field.";
		}
		
		return $answer;
	}
	
	public function printData()
	{			
		$str = parent::printData();
		$str .= "<p>Weight: {$this->weight} g</p>" . 
		"</div>";
		return $str;
	}
	
	public function submitData()
	{
		$conn = Database::connect();
		$result = $conn->query("SELECT id FROM type WHERE name = 'Book'");
		$type_id = $result->fetch_assoc()['id'];
		$sql = "INSERT INTO product (sku, name, price, type) VALUES ('{$this->sku}', '{$this->name}', {$this->price}, {$type_id})";
		$conn->query($sql);
		$sql = "INSERT INTO book (id, weight) VALUES ({$conn->insert_id}, {$this->weight})";
		$conn->query($sql);
	}
}

	