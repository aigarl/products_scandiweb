<?php
// Disc class type

require_once("Product.php");
require_once("Database.php");

class DVDDisc extends Product
{
	public $size;
	
	public function setExtraData($data)
	{
		$this->size = $data["size"];
	}
	
	public function askData()
	{
		$str = '
		<div class="form-group" id="size">
			<label class="control-label col-sm-3" for="size">Size</label>
			<div class="col-sm-9">
			<input type="text" class="form-control" name="size"></input>
			<span class="help-block error"></span>
			</div>
		</div>
		<div class="additional_info">
			Please, type the size of the DVD Disc in megabytes.
		</div>
		';
		return $str;
	}
	
	public function checkData(& $data)
	{
		$answer = parent::checkData($data);
		
		if (!empty($data["size"])) {
			$this->size = $this->cleanData($data["size"]);
			if (!is_numeric($this->size)) {
				$answer["size"] = "Size should be numeric.";
			} else if ($this->size < 0) {
				$answer["size"] = "Size can't be negative";
			}
		} else {
			$answer["size"] = "Obligatory field.";
		}
		
		return $answer;
	}
	
	public function printData()
	{			
		$str = parent::printData();
		$str .= "<p>Size: {$this->size} MB</p>" .
		"</div>";
		return $str;
	}
	
	public function submitData()
	{
		$conn = Database::connect();
		$result = $conn->query("SELECT id FROM type WHERE name = 'DVDDisc'");
		$type_id = $result->fetch_assoc()['id'];
		$sql = "INSERT INTO product (sku, name, price, type) VALUES ('{$this->sku}', '{$this->name}', {$this->price}, {$type_id})";
		$conn->query($sql);
		$sql = "INSERT INTO dvddisc (id, size) VALUES ({$conn->insert_id}, {$this->size})";
		$conn->query($sql);
	}
}