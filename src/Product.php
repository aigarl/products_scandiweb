<?php
// Abstract class for Product datatype
	
abstract class Product
{
	public $sku;
	public $name;
	public $price;
	
	public function setExtraData($data) {}
	
	public function askData() {}
	
	public function checkData(& $data)
	{
		$answer = [];
		
		if (!empty($data["sku"])) {
			$this->sku = $this->cleanData($data["sku"]);
			$this->sku = strtoupper($this->sku);
			if (!preg_match("/^[A-Z0-9-]*$/", $this->sku)) {
				$answer["sku"] = "SKU can contain only latin letters, numbers and dashes.";
			}
		} else {
			$answer["sku"] = "Obligatory field.";
		}
		
		
		if (!empty($data["name"])) {
			$this->name = $this->cleanData($data["name"]);
			$this->name = preg_replace('/\s+/', ' ', $this->name);
		} else {
			$answer["name"] = "Obligatory field.";
		}
		
		if (!empty($data["price"]) || $data["price"] == 0) {
			$this->price = $this->cleanData($data["price"]);
			if (is_numeric($this->price)) {
				if ($this->price < 0) {
					$answer["price"] = "Price can't be negative.";
				} else {
					$this->price = number_format($this->price, 2, ".", "");
				}
			} else {
				$answer["price"] = "Price should be numeric.";
			}
		} else {
			$answer["price"] = "Obligatory field.";
		}
		
		return $answer;
		
	}
	
	public function printData()
	{
		$str = "<div class=\"productinfo\">" .
		"<p>{$this->sku}</p>" .
		"<p>{$this->name}</p>" . 
		"<p>{$this->price}</p>";
		return $str;
		
	}
	
	public function submitData() {}
	
	protected function cleanData($data)
	{
		return htmlspecialchars(stripslashes(trim($data)));
	}
	
}