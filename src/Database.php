<?php
// Database connection wrapper

class Database
{
	private const HOSTNAME = "localhost:3306";
	private const USERNAME = "user";
	private const PASSWORD = "123456";
	private const DBNAME = "products_scandiweb";
	
	public static function connect()
	{
		return new mysqli(Database::HOSTNAME, Database::USERNAME, Database::PASSWORD, Database::DBNAME);
	}
	
}
