<?php
// Furniture class type

require_once("Product.php");
require_once("Database.php");

class Furniture extends Product
{
	public $width;
	public $height;
	public $length;
	
	public function setExtraData($data)
	{
		$this->width = $data["width"];
		$this->height = $data["height"];
		$this->length = $data["length"];
	}
	
	public function askData()
	{
		$str = '
		<div class="form-group" id="height">
			<label class="control-label col-sm-3" for="height">Height</label>
			<div class="col-sm-9">
			<input type="text" class="form-control" name="height"></input>
			<span class="help-block error"></span>
			</div>
		</div>
		<div class="form-group" id="width">
			<label class="control-label col-sm-3" for="width">Width</label>
			<div class="col-sm-9">
			<input type="text" class="form-control" name="width"></input>
			<span class="help-block error"></span>
			</div>
		</div>
		<div class="form-group" id="length">
			<label class="control-label col-sm-3" for="length">Length</label>
			<div class="col-sm-9">
			<input type="text" class="form-control" name="length"></input>
			<span class="help-block error"></span>	
			</div>
		</div>
		<div class="additional_info">
			Type the height, width, length of the furniture in meters.
		</div>
		';
		return $str;
	}
	
	public function checkData(& $data)
	{
		$answer = parent::checkData($data);
		
		if (!empty($data["width"])) {
			$this->width = $this->cleanData($data["width"]);
			if (!is_numeric($this->width)) {
				$answer["width"] = "Width should be numeric.";
			} else if ($this->width < 0) {
				$answer["width"] = "Width can't be negative";
			}
		} else {
			$answer["width"] = "Obligatory field.";
		}
		
		if (!empty($data["height"])) {
			$this->height = $this->cleanData($data["height"]);
			if (!is_numeric($this->height)) {
				$answer["height"] = "Height should be numeric.";
			} else if ($this->height < 0) {
				$answer["height"] = "Height can't be negative";
			}
		} else {
			$answer["height"] = "Obligatory field.";
		}
		
		if (!empty($data["length"])) {
			$this->length = $this->cleanData($data["length"]);
			if (!is_numeric($this->length)) {
				$answer["length"] = "Length should be numeric.";
			} else if ($this->length < 0) {
				$answer["length"] = "Length can't be negative";
			}
		} else {
			$answer["length"] = "Obligatory field.";
		}
	
		return $answer;
	}
	
	public function printData()
	{			
		$str = parent::printData();
		$str .= "<p>Dimensions: {$this->width} x {$this->height} x {$this->length}</p>" .
		"</div>";
		return $str;
	}
	
	public function submitData()
	{
		$conn = Database::connect();
		$result = $conn->query("SELECT id FROM type WHERE name = 'Furniture'");
		$type_id = $result->fetch_assoc()['id'];
		$sql = "INSERT INTO product (sku, name, price, type) VALUES ('{$this->sku}', '{$this->name}', {$this->price}, {$type_id})";
		$conn->query($sql);
		$sql = "INSERT INTO furniture (id, width, height, length) VALUES ({$conn->insert_id}, {$this->width}, {$this->height}, {$this->length})";
		$conn->query($sql);
		
	}
	
}