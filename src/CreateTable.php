<?php
// Function for creating a view with inforamtion about products in the database

require_once("Database.php");
require_once("Book.php");
require_once("DVDDisc.php");
require_once("Furniture.php");

function create_table()
{
	$type_objects = ["DVDDisc" => new DVDDisc(), "Book" => new Book(), "Furniture" => new Furniture()];
	
	$connection = Database::connect();
	
	$extra_data = [];
	foreach (array_keys($type_objects) as $type) {
		$extra_data[$type] = [];
		$result = $connection->query("SELECT * FROM " . strtolower($type));
		while($row = $result->fetch_assoc()) {
			$id = $row["id"];
			unset($row["id"]);
			$extra_data[$type][$id] = $row;
		}
	}
	
	$table = "";
	
	$result = $connection->query("SELECT product.id, product.sku, product.name, product.price, type.name as type FROM product JOIN type ON type.id = product.type");
	while ($row = $result->fetch_assoc()) {
		$table .= "<div class=\"col-sm-4 col-md-3 productcell\" data-pid=\"${row['id']}\"><div class=\"panel panel-default productpanel\">";
		$table .= "<div class=\"checkbox\"><input type=\"checkbox\" name=\"delete\" value=\"{$row['id']}\"></div>";
		$product_object = $type_objects[$row["type"]];
		$product_object->sku = $row["sku"];
		$product_object->name = $row["name"];
		$product_object->price = number_format($row["price"], 2, ".", "");
		$product_object->setExtraData($extra_data[$row["type"]][$row["id"]]);
		$table .= $product_object->printData() . "</div></div>";
	}
	
	
	return $table;
}